import argparse
import ipaddress
import socket
import ssl

parser = argparse.ArgumentParser()
parser.add_argument(
    "--start_ip", type=str, help="start IP", nargs="?", default="0.0.0.0", const=0
)
parser.add_argument(
    "--stop_ip", type=str, help="stop IP", nargs="?", default="255.255.255.255", const=0
)
parser.add_argument(
    "--timeout", type=float, help="connection timeout", nargs="?", default=1, const=0
)
args = parser.parse_args()


socket.setdefaulttimeout(args.timeout)

CURRENT_IP = args.start_ip
STOP_IP = args.stop_ip
# https://en.wikipedia.org/wiki/Reserved_IP_addresses
REPLACE_IPS = [
    ("0.0.0.0", "1.0.0.0"),
    ("10.0.0.0", "11.0.0.0"),
    ("100.64.0.0", "100.128.0.0"),
    ("127.0.0.0", "128.0.0.0"),
    ("169.254.0.0", "169.255.0.0"),
    ("172.16.0.0", "172.32.0.0"),
    ("192.0.0.0", "192.0.1.0"),
    ("192.0.2.0", "192.0.3.0"),
    ("192.88.99.0", "192.88.100.0"),
    ("192.168.0.0", "192.169.0.0"),
    ("198.18.0.0", "198.20.0.0"),
    ("198.51.100.0", "198.51.101.0"),
    ("203.0.113.0", "203.0.114.0"),
    ("224.0.0.0", "240.0.0.0"),
    ("233.252.0.0", "233.252.1.0"),
    ("240.0.0.0", "255.255.255.255"),
]


def get_next_ip(current_ip):
    if current_ip == REPLACE_IPS[0][0]:
        current_ip = REPLACE_IPS[0][1]
        REPLACE_IPS.pop(0)
        return current_ip
    return str(ipaddress.ip_address(current_ip) + 1)


def fetch_certs(start_ip, stop_ip):
    print(f"Start fetching certificates from {start_ip} to {stop_ip}")
    i = 0
    current_ip = start_ip
    while ipaddress.ip_address(current_ip) < ipaddress.ip_address(stop_ip):
        try:
            addr = (current_ip, 443)
            certificate = ssl.get_server_certificate(addr)
            with open("certs/" + current_ip + ".crt", "w") as f:
                f.write(certificate)
        except (socket.error, socket.timeout) as err:
            pass
        current_ip = get_next_ip(current_ip)

        i += 1
        if not i % 1500:
            print(current_ip)

    print(f"Generating over. Stopped at {current_ip}.")
    if current_ip != stop_ip:
        print(
            f"Program wasn't stopped at the stop. Current: {current_ip} | Stop: {stop_ip}"
        )


while ipaddress.ip_address(CURRENT_IP) > ipaddress.ip_address(REPLACE_IPS[0][1]):
    REPLACE_IPS.pop(0)
if ipaddress.ip_address(CURRENT_IP) > ipaddress.ip_address(REPLACE_IPS[0][0]):
    CURRENT_IP = REPLACE_IPS[0][1]

fetch_certs(CURRENT_IP, STOP_IP)
print("Done")
