package main

import (
	"bytes"
	"crypto/tls"
	"encoding/pem"
	"flag"
	"fmt"
	"net"
	"os"
	"time"
)

var REPLANE_IPS map[string]net.IP = map[string]net.IP{
	"0.0.0.0":      net.IPv4(1, 0, 0, 0),
	"10.0.0.0":     net.IPv4(11, 0, 0, 0),
	"100.64.0.0":   net.IPv4(100, 128, 0, 0),
	"127.0.0.0":    net.IPv4(128, 0, 0, 0),
	"169.254.0.0":  net.IPv4(169, 255, 0, 0),
	"172.16.0.0":   net.IPv4(172, 32, 0, 0),
	"192.0.0.0":    net.IPv4(192, 0, 1, 0),
	"192.0.2.0":    net.IPv4(192, 0, 3, 0),
	"192.88.99.0":  net.IPv4(192, 88, 100, 0),
	"192.168.0.0":  net.IPv4(192, 169, 0, 0),
	"198.18.0.0":   net.IPv4(198, 20, 0, 0),
	"198.51.100.0": net.IPv4(198, 51, 101, 0),
	"203.0.113.0":  net.IPv4(203, 0, 114, 0),
	"224.0.0.0":    net.IPv4(240, 0, 0, 0),
	"233.252.0.0":  net.IPv4(233, 252, 1, 0),
	"240.0.0.0":    net.IPv4(255, 255, 255, 255)}

func next_ip(ip net.IP) net.IP {
	res := ip.To4()
	for i := 3; i >= 0; i-- {
		res[i] = res[i] + 1
		if res[i] != 0 {
			break
		}
	}
	if val, ok := REPLANE_IPS[res.String()]; ok {
		return val
	}
	return res
}

func get_cert(domain, dir string, timeout int) {
	dialer := net.Dialer{
		Timeout: time.Duration(time.Duration(timeout) * time.Second),
	}
	conn, err := tls.DialWithDialer(&dialer, "tcp", domain+":443", &tls.Config{
		InsecureSkipVerify: true,
	})
	if err != nil {
		return
	}
	defer conn.Close()
	for i, cert := range conn.ConnectionState().PeerCertificates {
		var b bytes.Buffer
		err := pem.Encode(&b, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert.Raw,
		})
		if err != nil {
			fmt.Println("formatting error", err)
			return
		}
		err = os.WriteFile(fmt.Sprintf("%s/%s-%d.crt", dir, domain, i), b.Bytes(), 0777)
		if err != nil {
			fmt.Println("error writing file", err)
			return
		}
	}

}

func main() {
	goroutinesCount := flag.Int("goroutines", 10000, "the number of goroutines to use")
	start := flag.String("start", "1.0.0.0", "start at")
	stop := flag.String("stop", "", "stop at, leave empty to run indefinitely")
	dir := flag.String("certs-dir", "certs", "the directory in which to store the certificates")
	printEvery := flag.Int("print-every", 1000, "print the current domain every x domais")
	timeout := flag.Int("timeout", 4, "request timeout in seconds")
	flag.Parse()

	domains := make(chan string, 100)

	goroutine := func(domains chan string) {
		for domain := range domains {
			get_cert(domain, *dir, *timeout)
		}
	}
	for i := 0; i < *goroutinesCount; i++ {
		go goroutine(domains)
	}

	i := 0
	current := net.ParseIP(*start)
	for current.String() != *stop {
		domains <- current.String()

		current = next_ip(current)

		i += 1
		if i%(*printEvery) == 0 {
			fmt.Println(current)
		}
	}
}
