# What is it for

Two ways to generate certificates by testing every single IPv4 on the port 443. It's easy to tweak to check more ports though.

## Usage

Since google made a much better job at this than us, here is the link https://github.com/google/certificate-transparency-go
// Still need to explain how to use their thing since they can't be bothered to explain it.

# Use the certificate-transparency repo to get certificates

1. Clone the repo

```bash
git clone https://github.com/google/certificate-transparency-go.git
```

2. go to the scanner/scanlog folder

3. run the code

```bash
go run scanlog.go --dump_dir certificates --num_workers 500 --parallel_fetch 10000
```

You can use more workers / parallel fetch depending on your computer

You can use the `--start_index` option to restart where you left a previous scan

For a complete list of options, run

```bash
go run scanlog.go -h
```

4. wait
