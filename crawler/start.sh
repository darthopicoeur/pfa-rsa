for a in $(seq 86 100); do
    for b in $(seq 0 254); do
        python3 certs_gen.py --start_ip "$a.$b.0.0" --stop_ip "$a.$((b+1)).0.0" &
    done
done
