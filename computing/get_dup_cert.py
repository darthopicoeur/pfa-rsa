# search for all duplicated certificates in a folder and reference them in a file

import hashlib
from glob import glob
from os import path

def start():
    print("Enter path to folder")
    folderPath = input()
    dict_duplicate = {}
    t = 0
    
    files = glob(folderPath + "/*.crt") + glob(folderPath + "/*.pem")
    n = len(files)
    for file in files:
        cert = open(file, 'r').read()
        h = hashlib.sha256(cert.encode()).hexdigest()
        if h not in dict_duplicate.keys():
            dict_duplicate[h] = [path.basename(file)]
        else:
            dict_duplicate[h].append(path.basename(file))
        t+=1
        if t % 1000 == 0:
            print(t * 100 / n)

    out_file_dup = ""
    out_file_non_dup = ""
    for key in dict_duplicate.keys():
        if len(dict_duplicate[key]) > 1:
            out_file_dup += '\n'.join(dict_duplicate[key][1:]) + '\n'
        out_file_non_dup += dict_duplicate[key][0] + '\n'

    with open(folderPath + "/dup_certs.txt", 'w') as file:
        file.write(out_file_dup)
    with open(folderPath + "/non_dup_certs.txt", "w") as file:
        file.write(out_file_non_dup)
if __name__ == "__main__":
    start()

