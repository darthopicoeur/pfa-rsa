import sys
import os

def clean_duplicate(directory):
    with open(directory + "dup_certs.txt", "r") as file:
        dup_files = file.read().split('\n')[:-1]
    t = 0
    n = len(dup_files)
    for dup_file in dup_files:
        os.remove(directory + dup_file)
        t += 1
        if t % 1000 == 0:
            print(t * 100 / n)
    
    print("cleaned {} certificates".format(t))

def main(argv):
    clean_duplicate(argv[0])

if __name__ == "__main__":
    main(sys.argv[1:])