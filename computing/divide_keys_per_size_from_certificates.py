import sys
import time
from os.path import join
from cryptography.x509 import load_pem_x509_certificate
from cryptography.hazmat.primitives.asymmetric.ec import EllipticCurvePublicKey

ALL_KEY_SIZES = [4096, 2048, 1024, 3072, 4069, 1039, 8192, 4086, 512, 1040, 1536, 768]

def divide_all_per_size(directory_in):
    public_key_string_out_per_size = {}
    for key_size in ALL_KEY_SIZES:
        public_key_string_out_per_size[key_size] = []
    string_name_certificate_out = ""
    i=0
    t=0
    tot_elliptic = 0
    prev_time = time.time()

    f = open(join(directory_in, "non_dup_certs.txt"), "r")
    files = f.read().split('\n')[:-1]
    f.close()
    print("Appending certificates for {} files.".format(len(files)))
    n = len(files)
    for one_file in files:
        fp = open(join(directory_in, one_file), 'r')
        try:
            cert_obj = load_pem_x509_certificate(fp.read().encode('utf-8'))
            if isinstance(cert_obj.public_key(), EllipticCurvePublicKey):
                # print("elliptic public key for {}".format(one_file))
                tot_elliptic += 1
            else:
                if cert_obj.public_key().key_size not in public_key_string_out_per_size.keys():
                    public_key_string_out_per_size[cert_obj.public_key().key_size] = []
                public_key_string_out_per_size[cert_obj.public_key().key_size].append("{},{}".format(i, cert_obj.public_key().public_numbers().n))
                string_name_certificate_out += "{},{}\n".format(i, one_file.split('/')[-1])
                i += 1
            t += 1
            if t % 1000 == 0:
                print(time.time()-prev_time)
                prev_time=time.time()
                print(t * 100/n)
        except:
            print("somethign went wrong with {}".format(one_file))
    
    fp = open(directory_in + "name_certificate_csv.txt", "w")
    fp.write(string_name_certificate_out)
    fp.close()

    for size in public_key_string_out_per_size.keys():
        fp = open(directory_in + "public_keys_csv_{}.txt".format(size), "w")
        fp.write('\n'.join(public_key_string_out_per_size[size]))
        fp.close()
    print("{} elliptic key".format(tot_elliptic))

def main(argv):
    for directory in argv:
        prev_time = time.time()
        divide_all_per_size(directory)
        middle_time = time.time()

        print("Took {} seconds to group for {} directory.".format(middle_time - prev_time, directory))


if __name__ == "__main__":
    main(sys.argv[1:])