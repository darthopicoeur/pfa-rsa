import sys
import os
import shutil

def add_certificate_folders_in_merged(directories):
    destination_folder = "crawler/merged_folder_certificate/"
    for directory in directories:
        t = 0
        n = len(os.listdir(directory))
        # fetch all files
        for file_name in os.listdir(directory):
            # construct full file path
            source = directory + file_name
            destination = destination_folder + file_name
            # copy only certificate files
            if os.path.isfile(source) and (os.path.splitext(file_name)[1] == '.crt' or os.path.splitext(file_name)[1] == '.pem'):
                shutil.copy(source, destination)
            t += 1
            if t % 1000 == 0:
                print(t * 100 / n)
        print("Finished directory {}".format(directory))

def main(argv):
    add_certificate_folders_in_merged(argv)

if __name__ == "__main__":
    main(sys.argv[1:])