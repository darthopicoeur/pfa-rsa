variable "keys_count" {
  type    = number
  default = 10000
}

variable "key_size" {
  type    = number
  default = 512
}

variable "bucket_name" {
  type    = string
  default = "epita-pfa-rsa-project"
}

variable "instances_count" {
  type    = number
  default = 100
}

variable "instance_type" {
  type    = string
  default = "t3.nano"
}
