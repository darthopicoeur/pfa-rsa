terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.74"
    }
  }
  required_version = ">= 1.1.6"
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

locals {
  current_run = timestamp()
}

resource "aws_iam_policy" "bucket_policy" {
  name = "pfa-rsa-ec2-bucket-policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid    = "s3"
        Effect = "Allow",
        Action = [
          "s3:PutObject",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:ListObject"
        ],
        Resource = ["arn:aws:s3:::${var.bucket_name}/*", "arn:aws:s3:::${var.bucket_name}"]
      }
    ]
  })
}

resource "aws_iam_role" "instance_role" {
  name                = "pfa-rsa-instance-role"
  managed_policy_arns = [aws_iam_policy.bucket_policy.arn]

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "pfa-rsa-ec2-profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_s3_bucket_object" "python_script" {
  bucket = var.bucket_name
  key    = "generate.py"
  source = "../generate.py"
  etag   = filemd5("../generate.py")
}

resource "aws_s3_bucket_object" "python_requirements" {
  bucket = var.bucket_name
  key    = "requirements.txt"
  source = "../requirements.txt"
  etag   = filemd5("../requirements.txt")
}

resource "aws_spot_instance_request" "key_generator" {
  depends_on = [
    aws_s3_bucket_object.python_script,
    aws_s3_bucket_object.python_requirements
  ]

  count                = var.instances_count
  ami                  = "ami-0069d66985b09d219" # amazon linux 2
  instance_type        = var.instance_type
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  spot_type            = "one-time"

  user_data = <<EOF
#!/bin/bash

# download the python script, i'm too lazy to write a file provisioner
aws s3 cp s3://${var.bucket_name}/generate.py .
aws s3 cp s3://${var.bucket_name}/requirements.txt .

python3 -m pip install -r requirements.txt > /var/log/messages
python3 ./generate.py --count ${var.keys_count} --key-size ${var.key_size} --no-progressbar true

# upload the result to s3
aws s3 cp ./keys.csv s3://${var.bucket_name}/${local.current_run}/rsa-keys-${count.index}.csv

# terminate self
sudo shutdown now
EOF

  tags = {
    Type = "rsa-generator"
    Run  = local.current_run
  }
}
