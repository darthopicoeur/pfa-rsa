# What is it for

Generate a bunch of keys and check if there are any collisions in the certificates.

## Usage

Generate keys with

```bash
python generate.py
```

(run `python generate.py -h` for more infos)

install batch (gcd)[https://github.com/zugzwang/batchgcd] then run

```bash
./batchgcd keys.csv
```

## Infra

The infra folder contains Terraform files to boot up machines to generate more keys in parallel. 
