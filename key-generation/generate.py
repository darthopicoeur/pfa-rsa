import argparse
import csv
import OpenSSL
from tqdm import tqdm

parser = argparse.ArgumentParser(description="generate some rsa keys")
parser.add_argument('--count', '-c', type=int,
                    help="the number of keys to generate", default=100000)
parser.add_argument("--out", "-o", type=str,
                    help="the file in which the keys will be stored", default="keys.csv")
parser.add_argument("--key-size", "-k", default=512,
                    help="the size of the keys to generate", type=int)
parser.add_argument("--quiet", "-q", type=bool,
                    default=False, help="remove progress bar and status")
parser.add_argument("--no-progressbar",
                    help="remove the progress bar", default=False, type=bool)

args = parser.parse_args()

keys = []

if not args.quiet:
    print(f"generating {args.count} rsa keys\n")

for i in tqdm(range(args.count), disable=args.quiet or args.no_progressbar):
    # see https://www.pyopenssl.org/en/latest/api/crypto.html#pkey-objects
    key = OpenSSL.crypto.PKey()
    key.generate_key(
        OpenSSL.crypto.TYPE_RSA, args.key_size)

    private_numbers = key.to_cryptography_key().private_numbers()
    n = private_numbers.p * private_numbers.q

    # batchgcd expects hex values
    keys.append(format(n, "x"))

with open(args.out, "w") as f:
    writer = csv.writer(f)

    for i, key in enumerate(keys):
        writer.writerow([i, key])

if not args.quiet:
    print(f"\nwrote keys in {args.out}")
